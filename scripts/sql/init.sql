
CREATE TABLE public.boutique (
    id integer NOT NULL,
    average_cook_time double precision,
    name character varying(255),
    mall_id integer
);


ALTER TABLE public.boutique OWNER TO rest;

--
-- Name: eat_box; Type: TABLE; Schema: public; Owner: rest
--

CREATE TABLE public.eat_box (
    id integer NOT NULL,
    boutique_id integer
);


ALTER TABLE public.eat_box OWNER TO rest;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: rest
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO rest;

--
-- Name: location_history_entity; Type: TABLE; Schema: public; Owner: rest
--

CREATE TABLE public.location_history_entity (
    id integer NOT NULL,
    latitude double precision NOT NULL,
    longitute double precision NOT NULL,
    "time" double precision NOT NULL
);


ALTER TABLE public.location_history_entity OWNER TO rest;

--
-- Name: location_history_entity_id_seq; Type: SEQUENCE; Schema: public; Owner: rest
--

CREATE SEQUENCE public.location_history_entity_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.location_history_entity_id_seq OWNER TO rest;

--
-- Name: location_history_entity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rest
--

ALTER SEQUENCE public.location_history_entity_id_seq OWNED BY public.location_history_entity.id;


--
-- Name: mall; Type: TABLE; Schema: public; Owner: rest
--

CREATE TABLE public.mall (
    id integer NOT NULL,
    address character varying(255),
    latitude double precision,
    longitude double precision,
    name character varying(255)
);


ALTER TABLE public.mall OWNER TO rest;

--
-- Name: order_borat; Type: TABLE; Schema: public; Owner: rest
--

CREATE TABLE public.order_borat (
    id integer NOT NULL,
    arrival_estimate double precision NOT NULL,
    price bigint,
    "time" timestamp without time zone,
    payment_id integer
);


ALTER TABLE public.order_borat OWNER TO rest;

--
-- Name: order_borat_location_history; Type: TABLE; Schema: public; Owner: rest
--

CREATE TABLE public.order_borat_location_history (
    order_id integer NOT NULL,
    location_history_id integer NOT NULL
);


ALTER TABLE public.order_borat_location_history OWNER TO rest;

--
-- Name: order_borat_products; Type: TABLE; Schema: public; Owner: rest
--

CREATE TABLE public.order_borat_products (
    order_id integer NOT NULL,
    products_id integer NOT NULL
);


ALTER TABLE public.order_borat_products OWNER TO rest;

--
-- Name: payment; Type: TABLE; Schema: public; Owner: rest
--

CREATE TABLE public.payment (
    id integer NOT NULL,
    amount integer DEFAULT 0,
    latitude double precision,
    longitude double precision,
    "time" timestamp without time zone
);


ALTER TABLE public.payment OWNER TO rest;

--
-- Name: payment_id_seq; Type: SEQUENCE; Schema: public; Owner: rest
--

CREATE SEQUENCE public.payment_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_id_seq OWNER TO rest;

--
-- Name: payment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rest
--

ALTER SEQUENCE public.payment_id_seq OWNED BY public.payment.id;


--
-- Name: position_entity; Type: TABLE; Schema: public; Owner: rest
--

CREATE TABLE public.position_entity (
    id integer NOT NULL,
    count integer,
    eat_box_id integer,
    product_id integer
);


ALTER TABLE public.position_entity OWNER TO rest;

--
-- Name: position_entity_id_seq; Type: SEQUENCE; Schema: public; Owner: rest
--

CREATE SEQUENCE public.position_entity_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.position_entity_id_seq OWNER TO rest;

--
-- Name: position_entity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: rest
--

ALTER SEQUENCE public.position_entity_id_seq OWNED BY public.position_entity.id;


--
-- Name: product; Type: TABLE; Schema: public; Owner: rest
--

CREATE TABLE public.product (
    id integer NOT NULL,
    description character varying(255),
    image_url character varying(255),
    name character varying(255) NOT NULL,
    price bigint,
    short_description character varying(255) NOT NULL,
    product_group_id integer,
    provider_id integer
);


ALTER TABLE public.product OWNER TO rest;

--
-- Name: product_group; Type: TABLE; Schema: public; Owner: rest
--

CREATE TABLE public.product_group (
    id integer NOT NULL,
    name character varying(255)
);


ALTER TABLE public.product_group OWNER TO rest;

--
-- Name: location_history_entity id; Type: DEFAULT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.location_history_entity ALTER COLUMN id SET DEFAULT nextval('public.location_history_entity_id_seq'::regclass);


--
-- Name: payment id; Type: DEFAULT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.payment ALTER COLUMN id SET DEFAULT nextval('public.payment_id_seq'::regclass);


--
-- Name: position_entity id; Type: DEFAULT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.position_entity ALTER COLUMN id SET DEFAULT nextval('public.position_entity_id_seq'::regclass);


--
-- Data for Name: boutique; Type: TABLE DATA; Schema: public; Owner: rest
--

--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: rest
--

SELECT pg_catalog.setval('public.hibernate_sequence', 1, false);


--
-- Name: location_history_entity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rest
--

SELECT pg_catalog.setval('public.location_history_entity_id_seq', 1, false);


--
-- Name: payment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rest
--

SELECT pg_catalog.setval('public.payment_id_seq', 1, false);


--
-- Name: position_entity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: rest
--

SELECT pg_catalog.setval('public.position_entity_id_seq', 1, false);


--
-- Name: boutique boutique_pkey; Type: CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.boutique
    ADD CONSTRAINT boutique_pkey PRIMARY KEY (id);


--
-- Name: eat_box eat_box_pkey; Type: CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.eat_box
    ADD CONSTRAINT eat_box_pkey PRIMARY KEY (id);


--
-- Name: location_history_entity location_history_entity_pkey; Type: CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.location_history_entity
    ADD CONSTRAINT location_history_entity_pkey PRIMARY KEY (id);


--
-- Name: mall mall_pkey; Type: CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.mall
    ADD CONSTRAINT mall_pkey PRIMARY KEY (id);


--
-- Name: order_borat_location_history order_borat_location_history_pkey; Type: CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.order_borat_location_history
    ADD CONSTRAINT order_borat_location_history_pkey PRIMARY KEY (order_id, location_history_id);


--
-- Name: order_borat order_borat_pkey; Type: CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.order_borat
    ADD CONSTRAINT order_borat_pkey PRIMARY KEY (id);


--
-- Name: payment payment_pkey; Type: CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.payment
    ADD CONSTRAINT payment_pkey PRIMARY KEY (id);


--
-- Name: position_entity position_entity_pkey; Type: CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.position_entity
    ADD CONSTRAINT position_entity_pkey PRIMARY KEY (id);


--
-- Name: product_group product_group_pkey; Type: CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.product_group
    ADD CONSTRAINT product_group_pkey PRIMARY KEY (id);


--
-- Name: product product_pkey; Type: CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- Name: order_borat_location_history uk_31da6ggh8du968j0ehdm1fxvi; Type: CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.order_borat_location_history
    ADD CONSTRAINT uk_31da6ggh8du968j0ehdm1fxvi UNIQUE (location_history_id);


--
-- Name: order_borat_products uk_8xlotsbtlxnm9snjyimx6x01k; Type: CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.order_borat_products
    ADD CONSTRAINT uk_8xlotsbtlxnm9snjyimx6x01k UNIQUE (products_id);


--
-- Name: position_entity fk23l2akr72vi86ehawwkan1pjm; Type: FK CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.position_entity
    ADD CONSTRAINT fk23l2akr72vi86ehawwkan1pjm FOREIGN KEY (product_id) REFERENCES public.product(id);


--
-- Name: order_borat_location_history fk84lxxwm4llut6sk10f265l92l; Type: FK CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.order_borat_location_history
    ADD CONSTRAINT fk84lxxwm4llut6sk10f265l92l FOREIGN KEY (location_history_id) REFERENCES public.location_history_entity(id);


--
-- Name: boutique fk9qrd2jxiwrp9ioopy70jdp6ya; Type: FK CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.boutique
    ADD CONSTRAINT fk9qrd2jxiwrp9ioopy70jdp6ya FOREIGN KEY (mall_id) REFERENCES public.mall(id);


--
-- Name: product fkansvnl2nnqfxag3scekom2o0o; Type: FK CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT fkansvnl2nnqfxag3scekom2o0o FOREIGN KEY (provider_id) REFERENCES public.boutique(id);


--
-- Name: position_entity fkaoc4heda4xla6n2gr2p5mao6e; Type: FK CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.position_entity
    ADD CONSTRAINT fkaoc4heda4xla6n2gr2p5mao6e FOREIGN KEY (eat_box_id) REFERENCES public.eat_box(id);


--
-- Name: product fkd1puiblqvkggoc63q7c3ux5x6; Type: FK CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT fkd1puiblqvkggoc63q7c3ux5x6 FOREIGN KEY (product_group_id) REFERENCES public.product_group(id);


--
-- Name: order_borat_products fkd2u9tsajfu955mijd0pgwlviv; Type: FK CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.order_borat_products
    ADD CONSTRAINT fkd2u9tsajfu955mijd0pgwlviv FOREIGN KEY (products_id) REFERENCES public.position_entity(id);


--
-- Name: order_borat_products fkj3bbkscn9qwc4vi719mk4e8wl; Type: FK CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.order_borat_products
    ADD CONSTRAINT fkj3bbkscn9qwc4vi719mk4e8wl FOREIGN KEY (order_id) REFERENCES public.order_borat(id);


--
-- Name: order_borat fkjoeupybj787bu6m4y0gycli0u; Type: FK CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.order_borat
    ADD CONSTRAINT fkjoeupybj787bu6m4y0gycli0u FOREIGN KEY (payment_id) REFERENCES public.payment(id);


--
-- Name: eat_box fklcibufurw5jnw97n5ij1l6ys8; Type: FK CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.eat_box
    ADD CONSTRAINT fklcibufurw5jnw97n5ij1l6ys8 FOREIGN KEY (boutique_id) REFERENCES public.boutique(id);


--
-- Name: order_borat_location_history fks5clxsmv6onnup8fh4oxetdmk; Type: FK CONSTRAINT; Schema: public; Owner: rest
--

ALTER TABLE ONLY public.order_borat_location_history
    ADD CONSTRAINT fks5clxsmv6onnup8fh4oxetdmk FOREIGN KEY (order_id) REFERENCES public.order_borat(id);


--
-- PostgreSQL database dump complete
--



insert into mall (id, name, latitude, longitude, address, image_url) values
(1, 'Румба', 59.8798434, 30.2654813, 'ул. Васи Алексеева, 6', 'https://avatars.mds.yandex.net/get-altay/374295/2a0000015b1976167ac174694fae9d2946aa/XXXL'),
(2, 'Радуга',59.8687178, 30.3503221, 'пр. Космонавтов, 14', 'https://avatars.mds.yandex.net/get-pdb/251121/d5fcae4d-cd6a-4e86-a70f-ab783f7227bc/s1200?webp=false'),
(3, 'Охта Молл', 59.9401888,30.4180007,'Брантовская дор., 3', 'https://avatars.mds.yandex.net/get-pdb/202366/688af18c-eb0b-42eb-a90f-9f316d8e9f97/s1200?webp=false'),
(6, 'Magic Mamalyga', 59.940294, 30.323863, 'Большая Конюшенная улица, 5', 'https://fb.ru/media/i/2/5/7/6/3/i/25763.jpg'),
(7, 'Манеж', 59.933874, 30.314166, 'Исаакиевская площадь, 1', 'http://миамир.рф/uploadedfiles/1-062018/images/Konnogvardeysky_manege_in_SPB2.jpg'),
(5, 'Европолис', 59.9879191,30.3512751, 'Полюстровский пр., 84А', 'https://avatars.mds.yandex.net/get-pdb/202366/bb1838f2-6da1-4e0c-8fb9-795bd7a344f4/s1200?webp=false');
insert into boutique (id, mall_id, name) values
(1, 3, 'Додо пицца'),
(2, 3, 'KFC'),
(3, 1, 'McDonalds'),
(4, 5, 'SUBWAY'),
(6, 1, 'Мороженое batista'),
(7, 2, 'Chili Pizza'),
(8, 2, 'Piccolo'),
(10, 6, 'It’s Nice!')
(11, 7, 'It’s Nice!')
(9, 5, 'STARBUCKS');

insert into product_group (id, name) values
(1, 'Бургер'),
(2, 'Твистер'),
(3, 'Курица'),
(4, 'Пицца'),
(5, 'Салат'),
(6, 'Десерт'),
(7, 'Напиток'),
(8, 'Кофе');

insert into product (id, provider_id, product_group_id, name, price, short_description, image_url) values
(1, 2, 1, 'Чизбургер', 99, '', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRfhh2PTPenvVkDNnaVPvD9mca1JAxDM-DkdE3nWSXfphyUwAGc'),
(12, 3, 1, 'Чизбургер', 99, '', 'http://mirfactov.com/wp-content/uploads/hq-wallpapers_ru_food_849_1920x1200.jpg'),
(2, 2, 1, 'Шефбургер Де Люкс', 144, '', 'http://macburger51.ru/files/products/shefburger-de-lyuks-v-kfc.800x600w.png'),
(3, 3, 2, 'Твистер острый', 159, '', 'http://www.kfc-menu-i-ceny.ru/photos/normal/tvister---ostryy-165-966.jpg'),
(4, 7, 4, 'Пепперони', 180, '', 'http://www.kfc-menu-i-ceny.ru/photos/normal/tvister---ostryy-165-966.jpg'),
(11, 1, 4, 'Пепперони', 180, '', 'http://juliapizza.kz/pictures/product/big/4490_big.jpg'),
(5, 8, 5, 'Салат Греческий', 175, '', 'https://art-lunch.ru/content/uploads/2018/07/Greek_salad_01.jpg'),
(6, 8, 6, 'Пирожное Мозаикa', 75, '', 'http://edoza.ru/assets/images/products/2143/mosaic.jpg'),
(7, 9, 7, 'Coca-Colа', 95, '', 'https://www.coca-cola.ru/images/product-mobile/bottle_red.png'),
(8, 1, 6, 'Пицца-роллы с ананасом и голубым сыром', 175, '', 'http://www.kfc-menu-i-ceny.ru/photos/normal/tvister---ostryy-165-966.jpg'),
(9, 9, 8, 'Капучино', 185, '', 'https://s2.eda.ru/StaticContent/Photos/140812180013/140822212354/p_O.jpg'),
(13, 4, 8, 'Капучино', 185, '', 'https://static.1000.menu/img/content/4100/kofe-kapuchino_max_989.jpg'),
(10, 9, 8, 'Флэт Уайт', 200, '', 'https://azbukakofe.ru/image/cache/data/napitki/flat-300x300.png'),
(98, 11, 1, 'Бешпармак', 112, 'Nice', 'https://im0-tub-ru.yandex.net/i?id=31466772350fefb3ef4e841ddba6c9e1-l&n=13');