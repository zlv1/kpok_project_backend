package org.borat;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Borat {
  public static final Logger logger = LogManager.getLogger(Borat.class);

  @RequestMapping(value = "/")
  public String getIndex() {
    logger.debug("main page");
    return "¯\\_(ツ)_/¯ ";
  }
}
