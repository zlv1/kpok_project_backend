package org.borat.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class InMallRequest {
  private Location location;
  private int mallId;

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public int getMallId() {
    return mallId;
  }

  public void setMallId(int mallId) {
    this.mallId = mallId;
  }
}
