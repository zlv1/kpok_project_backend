package org.borat.requests;

import org.borat.response.ProductResponse;

public class Position {

  private Integer productId;
  private Integer count;

  public Position(Integer productId, Integer count) {
    this.productId = productId;
    this.count = count;
  }

  public Integer getProductId() {
    return productId;
  }

  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  public Integer getCount() {

    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }
}
