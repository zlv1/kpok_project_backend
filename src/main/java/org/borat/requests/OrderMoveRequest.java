package org.borat.requests;

import org.borat.entities.Order;

public class OrderMoveRequest {
  Location location;
  Integer id;

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }
}
