package org.borat.requests;

import org.borat.entities.Order;

public class UpdateOrderRequest {
  private Integer id;
  private OrderRequest order;

  public UpdateOrderRequest(Order order) {
    id = order.getId();
    this.order = new OrderRequest(order);
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public OrderRequest getOrder() {
    return order;
  }

  public void setOrder(OrderRequest order) {
    this.order = order;
  }
}
