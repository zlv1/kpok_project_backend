package org.borat.requests;

import org.borat.entities.Order;
import org.borat.repositories.LocationHistoryEntityRepository;
import org.borat.repositories.PositionEntityRepository;
import org.borat.repositories.ProductRepository;

import java.util.List;
import java.util.stream.Collectors;

public class OrderRequest {
  private Location location;
  private String status;
  private List<Position> positionList;
  private String priority;

  public Order toEntity(
      ProductRepository productRepository,
      LocationHistoryEntityRepository locationHistoryEntityRepository,
      PositionEntityRepository positionEntityRepository) {
    return new Order(
        productRepository,
        locationHistoryEntityRepository,
        positionEntityRepository,
        location,
        positionList);
  }

  public OrderRequest() {}

  public OrderRequest(Order order) {
    location = null;
    status = order.getStatus();
    priority = order.getPriority();
    positionList =
        order
            .getProducts()
            .stream()
            .map(positionEntity -> new Position(positionEntity.getId(), positionEntity.getCount()))
            .collect(Collectors.toList());
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public List<Position> getPositionList() {
    return positionList;
  }

  public void setPositionList(List<Position> positionList) {
    this.positionList = positionList;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getPriority() {
    return priority;
  }

  public void setPriority(String priority) {
    this.priority = priority;
  }
}
