package org.borat.requests;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Location {

  private Double longitude;
  private Double latitude;

  @JsonCreator
  public Location(
      @JsonProperty(value = "longitude", required = true) Double longitude,
      @JsonProperty(value = "latitude", required = true) Double latitude) {
    this.longitude = longitude;
    this.latitude = latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  double degreesToRadians(double degrees) {
    return degrees * Math.PI / 180;
  }

  public Double distance(Location input) {
    double lat1 = input.getLatitude();
    double lon1 = input.longitude;
    double lat2 = latitude;
    double lon2 = longitude;

    double earthRadiusKm = 6371;

    double dLat = degreesToRadians(lat2 - lat1);
    double dLon = degreesToRadians(lon2 - lon1);

    lat1 = degreesToRadians(lat1);
    lat2 = degreesToRadians(lat2);

    double a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2)
            + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    return earthRadiusKm * c;
  }

  /* The function to convert decimal into radians */
  private double deg2rad(double deg) {
    return (deg * Math.PI / 180.0);
  }

  /* The function to convert radians into decimal */
  private double rad2deg(double rad) {
    return (rad * 180.0 / Math.PI);
  }
}
