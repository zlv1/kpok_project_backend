package org.borat.response;

import org.borat.entities.Mall;
import org.borat.requests.Location;

public class MallResponse {
  private Integer id;
  private String name;
  private Double latitude;
  private Double longitude;
  private String address;
  private Double distance;
  private String imageUrl;

  public MallResponse(Mall mall) {
    init(
        mall.getId(),
        mall.getName(),
        mall.getLatitude(),
        mall.getLongitude(),
        mall.getAddress(),
        mall.getImageUrl());
  }

  public MallResponse(Mall mall, Location location) {
    init(
        mall.getId(),
        mall.getName(),
        mall.getLatitude(),
        mall.getLongitude(),
        mall.getAddress(),
        mall.getImageUrl());
    distance = mall.distance(location);
  }

  private void init(
      Integer id, String name, Double latitude, Double longitude, String address, String imageUrl) {
    this.id = id;
    this.name = name;
    this.latitude = latitude;
    this.longitude = longitude;
    this.address = address;
    this.imageUrl = imageUrl;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Double getDistance() {
    return distance;
  }

  public void setDistance(Double distance) {
    this.distance = distance;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }
}
