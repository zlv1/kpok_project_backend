package org.borat.response;

import org.borat.entities.Product;

public class ProductResponse {
  private String description;
  private Integer id;
  private String name;
  private String imageUrl;
  private Long price;
  private Integer type;
  private String typeName;

  ProductResponse(Product food) {
    this.id = food.getId();
    this.imageUrl = food.getImageUrl();
    this.name = food.getName();
    this.price = food.getPrice();
    this.type = food.getProductGroup().getId();
    this.typeName = food.getProductGroup().getName();
    this.description = food.getDescription();
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Long getPrice() {
    return price;
  }

  public void setPrice(Long price) {
    this.price = price;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public String getTypeName() {
    return typeName;
  }

  public void setTypeName(String typeName) {
    this.typeName = typeName;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
