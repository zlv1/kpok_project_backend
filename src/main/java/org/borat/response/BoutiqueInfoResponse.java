package org.borat.response;

import org.borat.entities.Boutique;

import java.util.List;
import java.util.stream.Collectors;

public class BoutiqueInfoResponse {
  private String averageCookTime;
  private String name;
  private List<ProductResponse> productResponse;

  public BoutiqueInfoResponse(Boutique boutique) {
    this.name = boutique.getName();
    averageCookTime = boutique.getAverageCookTime();
    productResponse =
        boutique.getProducts().stream().map(ProductResponse::new).collect(Collectors.toList());
  }

  public String getAverageCookTime() {
    return averageCookTime;
  }

  public void setAverageCookTime(String averageCookTime) {
    this.averageCookTime = averageCookTime;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<ProductResponse> getProductResponse() {
    return productResponse;
  }

  public void setProductResponse(List<ProductResponse> productResponse) {
    this.productResponse = productResponse;
  }
}
