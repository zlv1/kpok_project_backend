package org.borat.repositories;

import org.borat.entities.LocationHistoryEntity;
import org.borat.entities.PositionEntity;
import org.springframework.data.repository.CrudRepository;

public interface PositionEntityRepository extends CrudRepository<PositionEntity, Integer> {
  PositionEntity findByid(Integer id);
}
