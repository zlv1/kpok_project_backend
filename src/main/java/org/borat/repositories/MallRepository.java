package org.borat.repositories;

import org.borat.entities.Mall;
import org.springframework.data.repository.CrudRepository;

public interface MallRepository extends CrudRepository<Mall, Integer> {
  Mall findByid(Integer id);
}
