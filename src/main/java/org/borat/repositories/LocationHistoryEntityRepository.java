package org.borat.repositories;

import org.borat.entities.Boutique;
import org.borat.entities.LocationHistoryEntity;
import org.springframework.data.repository.CrudRepository;

public interface LocationHistoryEntityRepository
    extends CrudRepository<LocationHistoryEntity, Integer> {
  LocationHistoryEntity findByid(Integer id);
}
