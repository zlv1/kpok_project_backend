package org.borat.repositories;

import org.borat.entities.Boutique;
import org.borat.entities.Mall;
import org.springframework.data.repository.CrudRepository;

public interface BoutiqueRepository extends CrudRepository<Boutique, Integer> {
  Boutique findByid(Integer id);
}
