package org.borat.repositories;

import org.borat.entities.ProductGroup;
import org.springframework.data.repository.CrudRepository;

public interface ProductGroupRepository extends CrudRepository<ProductGroup, Integer> {
  ProductGroup findByid(Integer id);
}
