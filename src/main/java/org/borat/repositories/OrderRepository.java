package org.borat.repositories;

import java.util.Set;
import org.borat.entities.Mall;
import org.borat.entities.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Integer> {
  Order findByid(Integer id);

  Set<Order> findByStatus(String status);
}
