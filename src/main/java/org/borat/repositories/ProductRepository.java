package org.borat.repositories;

import org.borat.entities.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Integer> {
  Product findByid(Integer id);
}
