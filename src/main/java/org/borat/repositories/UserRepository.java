package org.borat.repositories;

import org.borat.entities.Boutique;
import org.borat.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {
  User findByid(String id);
}
