package org.borat.settings;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@SuppressWarnings("unused")
@Component
@Configuration
public class AppConfig {
  private static final Logger LOGGER = LogManager.getLogger(AppConfig.class);

  public AppConfig() {}
}
