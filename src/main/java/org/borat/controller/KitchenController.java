package org.borat.controller;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.borat.*;
import org.borat.entities.Boutique;
import org.borat.entities.Order;
import org.borat.entities.PositionEntity;
import org.borat.entities.Product;
import org.borat.entities.ProductGroup;
import org.borat.repositories.*;
import org.borat.requests.ChangeOrderStatusRequest;
import org.borat.requests.OrderRequest;
import org.borat.requests.UpdateOrderRequest;
import org.borat.settings.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@SuppressWarnings("unused")
@Controller
public class KitchenController extends RKeeperCallbackGrpc.RKeeperCallbackImplBase {
  private static final Logger LOGGER = LogManager.getLogger(KitchenController.class);
  private final RKeeperGrpc.RKeeperBlockingStub serviceStub;
  @Autowired private ProductRepository productRepository;
  @Autowired private ProductGroupRepository productGroupRepository;
  @Autowired private MallRepository mallRepository;
  @Autowired private OrderRepository orderRepository;
  @Autowired private BoutiqueRepository boutiqueRepository;
  @Autowired private LocationHistoryEntityRepository locationHistoryEntityRepository;
  @Autowired private PositionEntityRepository positionEntityRepository;

  public KitchenController(AppConfig appConfig) throws IOException {
    ServerBuilder<?> serverBuilder = ServerBuilder.forPort(50051);
    serverBuilder.addService(this);
    io.grpc.Server server = serverBuilder.build();
    server.start();
    ManagedChannel serviceChannel =
        ManagedChannelBuilder.forTarget("proxy:50052").usePlaintext().build();
    serviceStub = RKeeperGrpc.newBlockingStub(serviceChannel);
  }

  @RequestMapping(value = "/change_order_status", method = RequestMethod.POST)
  protected ResponseEntity changeOrderStatus(@RequestBody ChangeOrderStatusRequest input) {
    changeOrderStatus(input.getId(), input.getStatus(), input.getStatus().equals("STARTED"));
    return new ResponseEntity(HttpStatus.OK);
  }

  private void changeOrderStatus(Integer id, String status, boolean started) {
    Order byid = orderRepository.findByid(id);
    if (started) {
      AddOrder.Builder builder = AddOrder.newBuilder().setId(id);
      for (PositionEntity position : byid.getProducts()) {
        builder.addPosition(
            Position.newBuilder().setId(position.getId()).setCount(position.getCount()).build());
      }
      serviceStub.addOrder(builder.build());
    }
    orderRepository.save(byid.changeStatus(locationHistoryEntityRepository, status));
  }

  @RequestMapping(value = "/get_order_priority/{id}", method = RequestMethod.GET)
  protected ResponseEntity<String> getOrderPriority(@PathVariable Integer id) {
    return new ResponseEntity<>(orderRepository.findByid(id).getPriority(), HttpStatus.OK);
  }

  @RequestMapping(value = "/get_orders/{status}", method = RequestMethod.GET)
  protected ResponseEntity<Set<UpdateOrderRequest>> getActiveOrders(@PathVariable String status) {
    Iterable<Order> byStatus;
    if (status.equals("ALL")) byStatus = orderRepository.findAll();
    else byStatus = orderRepository.findByStatus(status);
    Set<UpdateOrderRequest> collect =
        StreamSupport.stream(byStatus.spliterator(), false)
            .map(UpdateOrderRequest::new)
            .collect(Collectors.toSet());
    return new ResponseEntity<>(collect, HttpStatus.OK);
  }

  @Override
  public void changeStatus(
      ChangeStatus request, StreamObserver<ChangeStatus.Response> responseObserver) {
    changeOrderStatus(request.getId(), request.getStatus(), false);
    responseObserver.onNext(ChangeStatus.Response.newBuilder().setStatus(true).build());
    responseObserver.onCompleted();
  }

  @Override
  public void addProduct(
      AddOrChangeProduct request, StreamObserver<AddOrChangeProduct.Response> responseObserver) {
    ProductGroup productGroup = productGroupRepository.findByid(request.getProductGroupId());
    Boutique boutique = boutiqueRepository.findByid(request.getProviderId());
    productRepository.save(
        new Product(
            request.getId(),
            request.getName(),
            request.getShortDescription(),
            request.getShortDescription(),
            request.getImageUrl(),
            productGroup,
            boutique));
    responseObserver.onNext(AddOrChangeProduct.Response.newBuilder().setStatus(true).build());
    responseObserver.onCompleted();
  }
}
