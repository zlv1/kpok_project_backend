package org.borat.controller;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.borat.entities.User;
import org.borat.repositories.UserRepository;
import org.borat.settings.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

class LoginResponse {
  String token;

  public LoginResponse(String token) {
    this.token = token;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }
}

@SuppressWarnings("unused")
@Controller
public class LoginController {

  private static final Logger LOGGER = LogManager.getLogger(LoginController.class);
  private static final int TOKEN_LENGTH = 128;
  @Autowired private UserRepository userRepository;

  public LoginController(AppConfig appConfig) {}

  @RequestMapping(
      value = "/login/{id}",
      method = {RequestMethod.GET})
  protected ResponseEntity<LoginResponse> login(
      HttpServletRequest req, HttpServletResponse response, @PathVariable String id) {
    LOGGER.debug("Performing login");
    User user = userRepository.findByid(id);
    String token;
    token = RandomStringUtils.randomAlphanumeric(TOKEN_LENGTH);
    String sessionId = id + ":" + token;
    if (user == null) userRepository.save(new User(id, token));
    else user.update(token);

    return new ResponseEntity<>(new LoginResponse(token), HttpStatus.FOUND);
  }
}
