package org.borat.controller;

import org.borat.entities.Mall;
import org.borat.entities.Product;
import org.borat.repositories.BoutiqueRepository;
import org.borat.repositories.LocationHistoryEntityRepository;
import org.borat.repositories.MallRepository;
import org.borat.repositories.OrderRepository;
import org.borat.repositories.PositionEntityRepository;
import org.borat.repositories.ProductRepository;
import org.borat.requests.InMallRequest;
import org.borat.requests.Location;
import org.borat.requests.OrderMoveRequest;
import org.borat.requests.OrderRequest;
import org.borat.requests.UpdateOrderRequest;
import org.borat.response.BoutiqueInfoResponse;
import org.borat.response.MallResponse;
import org.borat.settings.AppConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@SuppressWarnings("unused")
@Controller
public class ClientController {
  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Autowired private ProductRepository productRepository;
  @Autowired private MallRepository mallRepository;
  @Autowired private OrderRepository orderRepository;
  @Autowired private BoutiqueRepository boutiqueRepository;
  @Autowired private LocationHistoryEntityRepository locationHistoryEntityRepository;
  @Autowired private PositionEntityRepository positionEntityRepository;

  public ClientController(AppConfig appConfig) {}

  @RequestMapping(value = "/get_malls", method = RequestMethod.GET)
  protected ResponseEntity<Set<MallResponse>> getMalls() {
    Iterable<Product> products = productRepository.findAll();
    Iterable<Mall> allMalls = mallRepository.findAll();

    Set<MallResponse> allMallsSet =
        StreamSupport.stream(allMalls.spliterator(), false)
            .map(MallResponse::new)
            .collect(Collectors.toSet());
    return new ResponseEntity<>(allMallsSet, HttpStatus.OK);
  }

  @RequestMapping(value = "/get_malls", method = RequestMethod.POST)
  protected ResponseEntity<Set<MallResponse>> getMalls(@RequestBody Location location) {
    Iterable<Product> products = productRepository.findAll();
    Iterable<Mall> allMalls = mallRepository.findAll();

    Set<MallResponse> allMallsSet =
        StreamSupport.stream(allMalls.spliterator(), false)
            .map(mall -> new MallResponse(mall, location))
            .collect(Collectors.toSet());
    return new ResponseEntity<>(allMallsSet, HttpStatus.OK);
  }

  static class BooleanResponse {
    private Boolean response;

    public BooleanResponse(Boolean response) {
      this.response = response;
    }

    public Boolean getResponse() {
      return response;
    }

    public void setResponse(Boolean response) {
      this.response = response;
    }
  }

  @RequestMapping(value = "/in_mall", method = RequestMethod.POST)
  protected ResponseEntity<BooleanResponse> orderMove(@RequestBody InMallRequest input) {
    Mall mall = mallRepository.findByid(input.getMallId());
    if (mall.near(input.getLocation()))
      return new ResponseEntity<>(new BooleanResponse(true), HttpStatus.OK);
    return new ResponseEntity<>(new BooleanResponse(false), HttpStatus.OK);
  }

  @RequestMapping(value = "/get_boutique_infos/{mallId}", method = RequestMethod.GET)
  protected ResponseEntity<Set<BoutiqueInfoResponse>> getBoutiqueInfos(
      @PathVariable Integer mallId) {
    return mallRepository
        .findById(mallId)
        .map(
            (mall) ->
                new ResponseEntity<>(
                    mall.getBoutiqueList()
                        .stream()
                        .map(boutique -> boutiqueRepository.findByid(boutique.getId()))
                        .filter(boutique -> boutique.getProducts().size() > 0)
                        .map(boutique -> new BoutiqueInfoResponse(boutique))
                        .collect(Collectors.toSet()),
                    HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @RequestMapping(value = "/order", method = RequestMethod.POST)
  protected ResponseEntity<OrderResponse> order(@RequestBody OrderRequest input) {
    return new ResponseEntity<>(
        new OrderResponse(
            orderRepository
                .save(
                    input.toEntity(
                        productRepository,
                        locationHistoryEntityRepository,
                        positionEntityRepository))
                .getId()),
        HttpStatus.OK);
  }

  @RequestMapping(value = "/update_order", method = RequestMethod.POST)
  protected ResponseEntity updateOrder(@RequestBody UpdateOrderRequest input) {
    orderRepository.save(
        orderRepository
            .findByid(input.getId())
            .update(
                productRepository,
                locationHistoryEntityRepository,
                positionEntityRepository,
                input.getOrder()));
    return new ResponseEntity<>(HttpStatus.OK);
  }

  @RequestMapping(value = "/order_move", method = RequestMethod.POST)
  protected ResponseEntity orderMove(@RequestBody OrderMoveRequest input) {
    orderRepository.save(
        orderRepository
            .findByid(input.getId())
            .move(locationHistoryEntityRepository, input.getLocation()));
    return new ResponseEntity(HttpStatus.OK);
  }

  private class OrderResponse {
    int id;

    public OrderResponse(Integer id) {
      this.id = id;
    }

    public int getId() {
      return id;
    }

    public void setId(int id) {
      this.id = id;
    }
  }
}
