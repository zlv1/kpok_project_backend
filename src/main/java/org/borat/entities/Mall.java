package org.borat.entities;

import javax.persistence.*;

import org.borat.requests.Location;

import javax.persistence.Entity;
import java.util.List;

@Entity
public class Mall {
  private static final double NEAR_LENGTH = 1.0;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String name;
  private Double latitude;
  private Double longitude;
  private String address;
  private String imageUrl;

  @OneToMany(mappedBy = "mall", cascade = CascadeType.ALL)
  private List<Boutique> boutiqueList;

  public Mall() {}

  public Mall(Integer id, String name, Double latitude, Double longitude, String address) {
    this.id = id;
    this.name = name;
    this.latitude = latitude;
    this.longitude = longitude;
    this.address = address;
  }

  public Double distance(Location input) { // killometers
    return new Location(longitude, latitude).distance(input);
  }

  public boolean near(Location input) {
    return distance(input) < NEAR_LENGTH;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Double getLatitude() {
    return latitude;
  }

  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }

  public Double getLongitude() {
    return longitude;
  }

  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public List<Boutique> getBoutiqueList() {
    return boutiqueList;
  }

  public void setBoutiqueList(List<Boutique> boutiqueList) {
    this.boutiqueList = boutiqueList;
  }

  public String getImageUrl() {
    return imageUrl;
  }
}
