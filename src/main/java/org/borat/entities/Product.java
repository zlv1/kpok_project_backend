package org.borat.entities;

import javax.persistence.*;

@Entity
public class Product {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private @Column(nullable = false) String name;
  private @Column(nullable = false) String shortDescription;
  private String description;
  private String imageUrl;
  private Long price;

  @ManyToOne private ProductGroup productGroup;

  @ManyToOne
  @JoinColumn(name = "provider_id")
  private Boutique provider;

  public Product() {}

  public Product(
      Integer id,
      String name,
      String shortDescription,
      String description,
      String imageUrl,
      ProductGroup productGroup,
      Boutique provider) {
    this.id = id;
    this.name = name;
    this.shortDescription = shortDescription;
    this.description = description;
    this.imageUrl = imageUrl;
    this.productGroup = productGroup;
    this.provider = provider;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getShortDescription() {
    return shortDescription;
  }

  public void setShortDescription(String shortDescription) {
    this.shortDescription = shortDescription;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public ProductGroup getProductGroup() {
    return productGroup;
  }

  public void setProductGroup(ProductGroup productGroup) {
    this.productGroup = productGroup;
  }

  public Boutique getProvider() {
    return provider;
  }

  public void setProvider(Boutique provider) {
    this.provider = provider;
  }

  public Long getPrice() {
    return price;
  }

  public void setPrice(Long price) {
    this.price = price;
  }
}
