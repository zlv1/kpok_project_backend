package org.borat.entities;

import javax.persistence.*;
import java.util.List;

@Entity
public class Boutique {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String name;

  @ManyToOne private Mall mall;

  @OneToMany(mappedBy = "provider")
  private List<Product> products;

  Double averageCookTime;

  public Boutique() {}

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Mall getMall() {
    return mall;
  }

  public void setMall(Mall mall) {
    this.mall = mall;
  }

  public List<Product> getProducts() {
    return products;
  }

  public String getAverageCookTime() {
    if (averageCookTime == null) {
      return null;
    }
    if (averageCookTime * 60 > 30) {
      return "high";
    }
    if (averageCookTime * 60 > 20) {
      return "middle";
    }
    return "low";
  }

  public void setProducts(List<Product> products) {
    this.products = products;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }
}
