package org.borat.entities;

import org.borat.requests.Location;
import org.borat.requests.Position;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class LocationHistoryEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private double longitute;
  private double latitude;
  private double time;
  private String status;

  public LocationHistoryEntity() {}

  public LocationHistoryEntity(Location location, String status) {
    longitute = location.getLongitude();
    latitude = location.getLatitude();
    time = new Date().getTime();
    this.status = status;
  }

  public Double distance(Location location) {
    return toLocation().distance(location);
  }

  Location toLocation() {
    return new Location(longitute, latitude);
  }

  public Double getTime() {
    return time;
  }

  public void setTime(double time) {
    this.time = time;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}
