package org.borat.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "userinfo")
public class User {
  @Id private String id;

  private long lastLogin;
  private String token;

  public User(String id, String token) {
    this.id = id;
    update(token);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public void update(String token) {
    this.token = token;
    lastLogin = new Date().getTime();
  }
}
