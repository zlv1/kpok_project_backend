package org.borat.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class EatBox {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @ManyToOne Boutique boutique;

  public EatBox() {}

  public EatBox(Integer id, Boutique boutique) {
    this.id = id;
    this.boutique = boutique;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Boutique getBoutique() {
    return boutique;
  }

  public void setBoutique(Boutique boutique) {
    this.boutique = boutique;
  }
}
