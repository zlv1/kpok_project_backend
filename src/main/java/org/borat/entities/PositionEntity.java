package org.borat.entities;

import org.borat.requests.Position;

import javax.persistence.*;

@Entity
public class PositionEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @ManyToOne private Product product;
  private Integer count;
  @ManyToOne private EatBox eatBox;

  public PositionEntity() {}

  public PositionEntity(Product product, Position position) {
    this.product = product;
    count = position.getCount();
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }
}
