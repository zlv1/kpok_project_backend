package org.borat.entities;

import javax.persistence.Entity;

import org.borat.repositories.LocationHistoryEntityRepository;
import org.borat.repositories.PositionEntityRepository;
import org.borat.repositories.ProductRepository;
import org.borat.requests.Location;
import org.borat.requests.OrderRequest;
import org.borat.requests.Position;

import java.util.*;
import java.util.stream.Collectors;
import javax.persistence.*;

@Entity
@Table(name = "order_borat")
public class Order {
  private static final boolean SIMPLE_IMPLEMENTATION = true;
  public static final String REQUESTED = "REQUESTED";

  @Id @GeneratedValue private Integer id;

  @OneToMany private List<PositionEntity> products;

  private Date time;
  private Long price;
  private String status;
  @OneToOne private Payment payment;

  private double arrivalEstimate; // hours

  @OneToMany Set<LocationHistoryEntity> locationHistory = new HashSet<>();

  public Order() {}

  public Order(
      ProductRepository productRepository,
      LocationHistoryEntityRepository locationHistoryEntityRepository,
      PositionEntityRepository positionEntityRepository,
      Location location,
      List<Position> positionList) {
    populateInfo(
        productRepository,
        locationHistoryEntityRepository,
        positionEntityRepository,
        location,
        positionList);
    if (getPriority().equals("POSTPONE")) status = REQUESTED;
    else status = "STARTED";
  }

  private void populateInfo(
      ProductRepository productRepository,
      LocationHistoryEntityRepository locationHistoryEntityRepository,
      PositionEntityRepository positionEntityRepository,
      Location location,
      List<Position> positionList) {
    Product byidGlobal = productRepository.findByid(positionList.get(0).getProductId());
    Mall mall = byidGlobal.getProvider().getMall();
    arrivalEstimate = mall.distance(location) / speed(location);
    products =
        positionList
            .stream()
            .map(
                position -> {
                  Product byid = productRepository.findByid(position.getProductId());
                  Boutique provider = byid.getProvider();
                  Mall mallCompare = provider.getMall();
                  if (!mallCompare.getId().equals(mall.getId()))
                    // mall id doesn't match
                    return null;
                  LocationHistoryEntity e = new LocationHistoryEntity(location, REQUESTED);
                  locationHistoryEntityRepository.save(e);
                  locationHistory.add(e);
                  PositionEntity entity =
                      new PositionEntity(
                          productRepository.findByid(position.getProductId()), position);
                  positionEntityRepository.save(entity);
                  return entity;
                })
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
  }

  public Order update(
      ProductRepository productRepository,
      LocationHistoryEntityRepository locationHistoryEntityRepository,
      PositionEntityRepository positionEntityRepository,
      OrderRequest input) {
    List<PositionEntity> productSaved = products;
    products = new LinkedList<>();
    productSaved.forEach(positionEntityRepository::delete);
    populateInfo(
        productRepository,
        locationHistoryEntityRepository,
        positionEntityRepository,
        input.getLocation(),
        input.getPositionList());
    return this;
  }

  public String getPriority() {
    if (arrivalEstimate * 60 > 60) {
      return "POSTPONE";
    }
    if (arrivalEstimate * 60 > 40) {
      return "LOW";
    }
    if (arrivalEstimate * 60 > 30) {
      return "MIDDLE";
    }
    return "HIGH";
  }

  public Order(Integer id, List<PositionEntity> products, Date time, Long price, Payment payment) {
    this.id = id;
    this.products = products;
    this.time = time;
    this.price = price;
    this.payment = payment;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public List<PositionEntity> getProducts() {
    return products;
  }

  public void setProducts(List<PositionEntity> products) {
    this.products = products;
  }

  public Date getTime() {
    return time;
  }

  public void setTime(Date time) {
    this.time = time;
  }

  public Long getPrice() {
    return price;
  }

  public void setPrice(Long price) {
    this.price = price;
  }

  public Payment getPayment() {
    return payment;
  }

  public void setPayment(Payment payment) {
    this.payment = payment;
  }

  private Double speed(Location location) { // km / hour
    if (SIMPLE_IMPLEMENTATION || locationHistory.size() < 1) {
      Double AVERAGE_SPEED = 30.0;
      return AVERAGE_SPEED;
    }
    LocationHistoryEntity previous =
        locationHistory
            .stream()
            .max((location1, location2) -> (int) (location1.getTime() - location2.getTime()))
            .orElse(null);
    return previous.distance(location)
        * 1000
        * 60
        * 60
        / (previous.getTime() - new Date().getTime());
  }

  public Order move(
      LocationHistoryEntityRepository locationHistoryEntityRepository, Location location) {
    Mall mall = products.get(0).getProduct().getProvider().getMall();
    arrivalEstimate = mall.distance(location) / speed(location);
    LocationHistoryEntity last =
        locationHistory
            .stream()
            .max((location1, location2) -> (int) (location1.getTime() - location2.getTime()))
            .orElse(null);
    LocationHistoryEntity e = new LocationHistoryEntity(location, last.getStatus());
    locationHistoryEntityRepository.save(e);
    locationHistory.add(e);
    return this;
  }

  public Order changeStatus(
      LocationHistoryEntityRepository locationHistoryEntityRepository, String status) {
    this.status = status;
    LocationHistoryEntity last =
        locationHistory
            .stream()
            .max((location1, location2) -> (int) (location1.getTime() - location2.getTime()))
            .orElse(null);
    LocationHistoryEntity e = new LocationHistoryEntity(last.toLocation(), status);
    locationHistoryEntityRepository.save(e);
    locationHistory.add(e);
    return this;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public double getArrivalEstimate() {
    return arrivalEstimate;
  }

  public void setArrivalEstimate(double arrivalEstimate) {
    this.arrivalEstimate = arrivalEstimate;
  }
}
